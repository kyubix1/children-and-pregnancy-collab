﻿namespace RimWorldChildren {
    using RimWorld;

    /// <summary>
    /// Def lib for CNP thoughts
    /// </summary>
    [DefOf]
    public class CnpThoughtDefOf {
        public static ThoughtDef NearParent;
        public static ThoughtDef ScaredOfTheDark;
        public static ThoughtDef GotToldOff;
        public static ThoughtDef GotDisciplined;
        public static ThoughtDef GotFed;

        public static ThoughtDef BabyGames;
        public static ThoughtDef JustBorn;
        public static ThoughtDef AdoptedMe;
        public static ThoughtDef GotHugged;
        public static ThoughtDef ColonyBirth;
        public static ThoughtDef ChildDied;
        public static ThoughtDef CryingBaby;
        public static ThoughtDef IAmPregnant;
        public static ThoughtDef PartnerIsPregnant;
        public static ThoughtDef WeGotPregnant;
        public static ThoughtDef WeHadBabies;
        public static ThoughtDef PostPartumDepression;
        public static ThoughtDef BabyStillborn;
        public static ThoughtDef IGaveBirthFirstTime;
        public static ThoughtDef IGaveBirth;
        public static ThoughtDef PartnerGaveBirth;
        public static ThoughtDef LateTermAbortion;
        public static ThoughtDef ChildExecuted;

        static CnpThoughtDefOf() {
            DefOfHelper.EnsureInitializedInCtor(typeof(CnpThoughtDefOf));
        }
    }
}
