﻿namespace RimWorldChildren {
    using RimWorld;
    using Verse;
    using Verse.Sound;

    public class Hediff_UnhappyBaby : HediffWithComps  {
        public override bool Visible {
            get {
                return false;
            }
        }

        private bool CanBabyCry() {
            return pawn.health.capacities.CapableOf(PawnCapacityDefOf.Breathing) && pawn.health.capacities.CanBeAwake;
        }

        public void WhineAndCry() {
            if (!IsBabyHungry() && !IsBabyUnhappy() && !IsBabyHurt()) {
                pawn.health.RemoveHediff (this);
            } else if (CanBabyCry()) {
                // Whine and cry
                MoteMaker.ThrowMetaIcon(pawn.Position, pawn.Map, ThingDefOf.Mote_IncapIcon);
                SoundInfo info = SoundInfo.InMap(new TargetInfo(pawn.PositionHeld, pawn.MapHeld));
                info.volumeFactor = ChildrenAndPregnancy.Settings.cryVolume;
                SoundDef.Named("Pawn_BabyCry").PlayOneShot(info);
            }
        }

        private bool IsBabyHurt() {
            return pawn.health.HasHediffsNeedingTend();
        }

        private bool IsBabyHungry() {
            return pawn.needs.food.CurLevelPercentage < pawn.needs.food.PercentageThreshHungry;
        }

        private bool IsBabyUnhappy() {
            return (pawn.needs?.joy?.CurLevelPercentage ?? 0f) < 0.2f;
        }

        public override void PostMake () {
            WhineAndCry();
            base.PostMake();
        }

        public override void Tick() {
            if (pawn.Spawned) {
                if (pawn.IsHashIntervalTick (1000)) {
                    LongEventHandler.ExecuteWhenFinished (delegate {
                        WhineAndCry ();
                    });
                }
            }
        }
    }
}