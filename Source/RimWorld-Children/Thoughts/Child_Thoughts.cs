﻿namespace RimWorldChildren {
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;

    public class ThoughtWorker_ScaredOfTheDark : ThoughtWorker_Dark {
        protected override ThoughtState CurrentStateInternal (Pawn p) {
            // Make sure it only gets applied to kids
            if (ChildrenUtility.GetLifestageType(p) != LifestageType.Child) {
                return false;
            }

            return p.Awake () && p.needs.mood.recentMemory.TicksSinceLastLight > 800;
        }
    }

    public class ThoughtWorker_NearParents : ThoughtWorker {
        private const int MAX_DISTANCE = 8;

        protected override ThoughtState CurrentStateInternal(Pawn p) {
            if (ChildrenUtility.GetLifestageType(p) > LifestageType.Toddler || !ChildrenUtility.RaceUsesChildren(p)) {
                return false;
            }

            Pawn mother = p.relations.GetFirstDirectRelationPawn(PawnRelationDefOf.Parent, x => x.gender == Gender.Female);
            Pawn father = p.relations.GetFirstDirectRelationPawn(PawnRelationDefOf.Parent, x => x.gender == Gender.Male);
            if (ArePawnsNear(p, mother) && ArePawnsNear(p, father)) {
                return ThoughtState.ActiveAtStage(2);
            }
            else if (ArePawnsNear(p, mother)) {
                return ThoughtState.ActiveAtStage(0);
            }
            else if (ArePawnsNear(p, father)) {
                return ThoughtState.ActiveAtStage(1);
            }
            else {
                return false;
            }
        }

        protected bool ArePawnsNear(Pawn a, Pawn b) {
            if (a == null || b == null) {
                return false;
            }

            return a.GetRoom() == b.GetRoom() && a.Position.DistanceTo(b.Position) < MAX_DISTANCE;
        }
    }
}