﻿
namespace RimWorldChildren {
    using RimWorld;

    [DefOf]
    public static class ChildBodyTypeDefOf {
        public static BodyTypeDef Child;
        public static BodyTypeDef Toddler;
        public static BodyTypeDef ToddlerUpright;
        public static BodyTypeDef Newborn;

        static ChildBodyTypeDefOf() {
            DefOfHelper.EnsureInitializedInCtor(typeof(BodyTypeDef));
        }

        /// <summary>
        /// Determines if the provided def matches one of the above bodytypes
        /// </summary>
        /// <param name="def">any body type def to compare</param>
        public static bool IsOfType(BodyTypeDef def) {
            return def == Child || def == Toddler || def == ToddlerUpright || def == Newborn;
        }
    }
}
