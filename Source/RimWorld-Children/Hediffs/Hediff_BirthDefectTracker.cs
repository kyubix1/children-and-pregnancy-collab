﻿namespace RimWorldChildren
{
    using System.Collections.Generic;
    using RimWorld;
    using Verse;

    public class Hediff_BirthDefectTracker : HediffWithComps
    {
        public float fetal_alcohol_ticker = 0;
        public bool fetal_alcohol = false;
        public bool stillbirth = false;
        public List<HediffDef> drug_addictions = new List<HediffDef>{};

        public void TickRare()
        {
            // Track if the person has drunk booze
            if (pawn.health.hediffSet.HasHediff (HediffDefOf.AlcoholHigh)) {
                fetal_alcohol_ticker += pawn.health.hediffSet.GetFirstHediffOfDef(HediffDefOf.AlcoholHigh).Severity;
            }
            if(fetal_alcohol == false){
                if (fetal_alcohol_ticker >= 5000 && fetal_alcohol_ticker < 10000) {
                    fetal_alcohol = true;
                }
                else {
                    stillbirth = true;
                }
            }
            // Track drugs taken
            foreach (Hediff hediff in pawn.health.hediffSet.hediffs) {
                if (hediff.sourceHediffDef.defName.EndsWith ("High")
                    && !drug_addictions.Contains(hediff.sourceHediffDef)
                    && !hediff.sourceHediffDef.defName.Contains("SmokeLeaf"))
                    drug_addictions.Add (hediff.sourceHediffDef);
            }
        }


        public override void Tick ()
        {
            if (pawn.IsHashIntervalTick(60)) {
                TickRare ();
            }
        }

        public override bool Visible {
            get { return false; }
        }
    }
}
